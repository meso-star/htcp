.\" Copyright (C) 2018, 2020-2023 |Méso|Star> (contact@meso-star.com)
.\" Copyright (C) 2018 Centre National de la Recherche Scientifique
.\" Copyright (C) 2018 Université Paul Sabatier
.\"
.\" This program is free software: you can redistribute it and/or modify
.\" it under the terms of the GNU General Public License as published by
.\" the Free Software Foundation, either version 3 of the License, or
.\" (at your option) any later version.
.\"
.\" This program is distributed in the hope that it will be useful,
.\" but WITHOUT ANY WARRANTY; without even the implied warranty of
.\" MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
.\" GNU General Public License for more details.
.\"
.\" You should have received a copy of the GNU General Public License
.\" along with this program. If not, see <http://www.gnu.org/licenses/>.
.Dd September 4, 2023
.Dt HTCP 5
.Os
.Sh NAME
.Nm htcp
.Nd High-Tune: Cloud Properties
.Sh DESCRIPTION
.Nm
is a binary file format for storing the properties of liquid water content
suspended in clouds.
Cloud properties are double-precision spatio-temporal floating-point values,
structured in a 4D grid whose definition and origin are stored in the
.Va definition
and
.Va lower-pos
fields.
The size of a grid cell can be irregular.
In such case, the
.Va is-Z-irregular
flag is set to 1 and the sizes of the cells in Z are explicitly listed in
.Va voxel-size
.Pq in meters .
The size of a grid cell along the X and Y axes is constant, whereas the size
along the Z axis may be irregular.
In this case, the
.Va is-Z-irregular
flag is set to 1 and the Z cell size is explicitly indicated in
.Va voxel-size
.Pq in meters .
.Pp
For each property, the list of its data is enumerated linearly along the X, Y,
Z and time dimensions, in that order.
The address where its first data is stored is aligned with the value defined by
the
.Va pagesize
field; several padding bytes can therefore be added before a property to ensure
data alignment.
Padding bytes are also added at the end of the file to align its overall size
with the size of a page.
.Pp
The stored cloud properties are as follows:
.Bl -dash -offset indent -compact
.It
.Va RVT :
water vapor mixing ratio in kg of water per m^3 of dry air.
.It
.Va RCT :
liquid water in suspension mixing ratio in kg of water per m^3 of dry air.
.It
.Va PABST :
pressure in Pascal.
.It
.Va T :
temperature in Kelvin.
.El
.Pp
Data are encoded with respect to the little endian bytes ordering, i.e. least
significant bytes are stored first.
.Pp
The file format is as follows:
.Bl -column (is-Z-irregular) (::=) ()
.It Ao Va htcp Ac Ta ::= Ta Ao Va pagesize Ac Ao Va is-Z-irregular Ac
.It Ta Ta Aq Va definition
.It Ta Ta Aq Va lower-pos
.It Ta Ta Aq Va voxel-size
.It Ta Ta Aq Va padding
.It Ta Ta Aq Va RVT
.It Ta Ta Aq Va padding
.It Ta Ta Aq Va RCT
.It Ta Ta Aq Va padding
.It Ta Ta Aq Va PABST
.It Ta Ta Aq Va padding
.It Ta Ta Aq Va T
.It Ta Ta Aq Va padding
.It \  Ta Ta
.It Ao Va pagesize Ac Ta ::= Ta Vt uint64_t
.It Ao Va is-Z-irregular Ac Ta ::= Ta Vt int8_t
.It \  Ta Ta
.It Ao Va definition Ac Ta ::= Ta Ao Va X Ac Ao Va Y Ac Ao Va Z Ac Ao Va time Ac
.It Ao Va lower-pos Ac Ta ::= Ta Vt double double double
# In m
.It Ao Va voxel-size Ac Ta ::= Ta Vt double double double ...
# In m
.It Ao Va X Ac Ta ::= Ta Vt uint32_t
.It Ao Va Y Ac Ta ::= Ta Vt uint32_t
.It Ao Va Z Ac Ta ::= Ta Vt uint32_t
.It Ao Va time Ac Ta ::= Ta Vt uint32_t
.It \  Ta Ta
.It Ao Va RVT Ac Ta ::= Ta Vt double ...
.It Ao Va RCT Ac Ta ::= Ta Vt double ...
.It Ao Va PABST Ac Ta ::= Ta Vt double ...
.It Ao Va T Ac Ta ::= Ta Vt double ...
.It Ao Va padding Ac Ta ::= Ta Op Vt int8_t ...
.El
.Sh SEE ALSO
.Xr les2htcp 1
