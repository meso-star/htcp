.\" Copyright (C) 2018, 2020-2023 |Méso|Star> (contact@meso-star.com)
.\" Copyright (C) 2018 Centre National de la Recherche Scientifique
.\" Copyright (C) 2018 Université Paul Sabatier
.\"
.\" This program is free software: you can redistribute it and/or modify
.\" it under the terms of the GNU General Public License as published by
.\" the Free Software Foundation, either version 3 of the License, or
.\" (at your option) any later version.
.\"
.\" This program is distributed in the hope that it will be useful,
.\" but WITHOUT ANY WARRANTY; without even the implied warranty of
.\" MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
.\" GNU General Public License for more details.
.\"
.\" You should have received a copy of the GNU General Public License
.\" along with this program. If not, see <http://www.gnu.org/licenses/>.
.Dd September 5, 2023
.Dt LES2HTCP 1
.Os
.Sh NAME
.Nm les2htcp
.Nd convert cloud properties from netCDF to
.Xr htcp 5
.Sh SYNOPSIS
.Nm
.Op Fl cfhqv
.Op Fl m Ar float_to_meter
.Op Fl o Ar output
.Op Fl p Ar pagesize
.Fl i Ar netcdf
.Sh DESCRIPTION
.Nm
generates a
.Xr htcp 5
file from cloud properties stored in a netCDF file.
The variables expected in the submitted netCDF file are:
.Bl -dash -offset indent
.It
.Va W_E_direction
and
.Va S_N_direction :
one-dimensional list of the position at the center of each cell along the
west-east and south-north horizontal axis, respectively.
The mesh must be homogeneous: each cell must have the same width along each
axis.
The unit is assumed to be meters, but this can be adjusted via
the
.Fl m
option.
.It
.Va VLEV
or
.Va vertical_levels :
position at the center of each cell along the vertical axis.
The vertical mesh can be inhomogeneous, i.e. each cell can have a different
vertical extent.
At least one of these variables must be defined.
Note that
.Va VLEV
is a four-dimensional variable, whereas
.Va vertical_levels
is assumed to be one-dimensional.
In all cases
.Nm
assumes that the vertical columns are
the same for each cell along the west-east and south-north axes.
The unit is assumed to be meters, but this can be adjusted via the
.Fl m
option.
.It
.Va RCT :
mixing ratio of liquid suspended water in each grid cell; in kg of water per kg
of dry air.
.It
.Va PABST :
pressure in each grid cell in Pascal.
.It
.Va THT :
potential temperature in each grid cell in Kelvin.
.El
.Pp
The options are as follows:
.Bl -tag -width Ds
.It Fl c
Advanced checks of the validity of the input
.Ar netcdf
file against
.Nm
prerequisites on netCDF data.
Note that this option can significantly increase conversion time.
.It Fl f
Forces overwriting of
.Ar output
file.
.It Fl h
Display short help.
.It Fl i Ar netcdf
netCDF file to convert.
.It Fl m Ar float_to_meter
Scale factor to be applied to floating-point number
.Li 1.0 to convert it to meters. By default, it is set to
.Li 1 .
.It Fl o Ar output
Output file.
If not defined, data is written to standard output.
.It Fl p Ar pagesize
Page size in bytes on which htcp data will be aligned.
It must be a power of 2 and greater than or equal to the size of a system page,
which is the default value
.Pq see Xr sysconf 3 .
.It Fl q
Writes nothing to the output.
Can be used in conjunction with the
.Fl c
option to check only the validity of the input netCDF.
.It Fl v
Display the version number and exit.
.El
.Sh EXIT STATUS
.Ex -std
.Sh EXAMPLES
Convert the netCDF
.Pa clouds.nc
file.
The resulting
.Xr htcp 5
file is stored in the
.Pa cloud.htcp
file unless it already exists; in this case, an error is notified, the program
stops and the
.Pa cloud.htcp
file remains unchanged:
.Pp
.Dl les2htcp -i clouds.nc -o clouds.htcp
.Pp
Converts netCDF file
.Pa clouds_km.nc
to
.Xr htcp 5
format.
Use the
.Fl f
option to write the output file
.Pa clouds.htcp
even if it already exists.
The input file to be converted has its spatial unit in kilo-meters, whereas the
htcp file format assumes meters; use the
.Fl m Ar 1000
option to perform the conversion:
.Pp
.Dl les2htcp -i clouds_km.nc -m 1000 -o clouds.htcp
.Pp
Check that the netCDF file
.Pa clouds.nc
is a valid input file for
.Nm .
Use the
.Fl q
option to disable file conversion:
.Pp
.Dl les2htcp -c -i clouds.nc -q
.Sh SEE ALSO
.Xr htrdr 1 ,
.Xr sysconf 3 ,
.Xr htcp 5
.Sh STANDARDS
.Rs
.%A Edward Hartnett
.%D March 2011
.%R ESDS-RFC-022v1
.%T netCDF4/HDF5 File Format
.Re
.Sh HISTORY
.Nm
has been developed to generate cloud properties as input to the
.Xr htrdr 1
program.
