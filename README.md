# High-Tune: Cloud Properties

This project defines a file format for storing the properties of liquid
water suspended in clouds. See `htcp.5` for the format specification. A
C library is provided for loading files in this format.

To facilitate interoperability, the `les2htcp` tool converts data to
htcp format from a
[netCDF](https://www.unidata.ucar.edu/software/netcdf/) file. While the
netCDF format is widely adopted there is no standard or common practice
for describing cloud properties, whether in terms of data layout,
variable names or units. The htcp format was developed to circumvent
this problem: it is a simple file format, independent of netCDF-based
data formatting, leaving data conversion to third-party translation
tools. `les2htcp` is one such tool. See `les2htcp.1` for a complete
descritpion.

## Requirements

- C compiler
- POSIX make
- pkg-config
- netCDF4
- [RSys](https://gitlab.com/vaplv/rsys)
- [mandoc](https://mandoc.bsd.lv)

## Installation

Edit config.mk as needed, then run:

    make clean install

## Release notes

### Version 0.1

- Write the man pages directly in mdoc's roff macros, instead of using
  the intermediate scdoc source.
- Replace CMake by Makefile as build system.
- Update compiler and linker flags to increase the security and
  robustness of generated binaries.
- Provide a pkg-config file to link the library as an external
  dependency.

### Version 0.0.5

Fix a typo in the les2htcp man page

### Version 0.0.4

- Use scdoc rather than asciidoc as file format for man sources.
- Make internal shell scripts POSIX compliant.

### Version 0.0.3

- Fix a man page error: the RCT field was named "liquid vapor mixing
  report" rather than "liquid _water_ mixing report".
- Miscellaneous minor corrections to log messages.

### Version 0.0.2

Fix the les2htcp man page. The units were incorrect for the water vapor
mixing ratio and the liquid water mixing ratio.

### Version 0.0.1

- Fix warnings and compilation errors when using the netCDF library in
  version 4.4.0.
- Fix compilation errors on systems with GNU C library version less than
  2.19.

## Copyright

Copyright (C) 2018, 2020-2023 |Méso|Star> (contact@meso-star.com)  
Copyright (C) 2018 Centre National de la Recherche Scientifique  
Copyright (C) 2018 Université Paul Sabatier

## License

`htcp` and `les2htcp` are free software released under the GPL v3+
license: GNU GPL version 3 or later. You are welcome to redistribute
them under certain conditions; refer to the COPYING file for details.
