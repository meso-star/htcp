# Copyright (C) 2018, 2020-2023 |Méso|Star> (contact@meso-star.com)
# Copyright (C) 2018 Centre National de la Recherche Scientifique
# Copyright (C) 2018 Université Paul Sabatier
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.

.POSIX:
.SUFFIXES: # Clean up default inference rules

include config.mk

LIBNAME_STATIC = libhtcp.a
LIBNAME_SHARED = libhtcp.so
LIBNAME = $(LIBNAME_$(LIB_TYPE))

################################################################################
# Library/program building
################################################################################
SRC = src/htcp.c
OBJ = $(SRC:.c=.o)
DEP = $(SRC:.c=.d)

default: build_library build_program

build_library: .config $(DEP)
	@$(MAKE) -fMakefile $$(for i in $(DEP); do echo -f $${i}; done) \
	$$(if [ -n "$(LIBNAME)" ]; then \
	     echo "$(LIBNAME)"; \
	   else \
	     echo "$(LIBNAME_SHARED)"; \
	   fi)

build_program: .config src/les2htcp.d build_library
	@$(MAKE) -fMakefile -fsrc/les2htcp.d les2htcp

$(DEP) $(OBJ): config.mk

$(LIBNAME_SHARED): $(OBJ)
	$(CC) $(CFLAGS_SO) $(RSYS_CFLAGS) -o $@ $(OBJ) $(LDFLAGS_SO) $(RSYS_LIBS)

$(LIBNAME_STATIC): libhtcp.o
	$(AR) -rc $@ $?
	$(RANLIB) $@

libhtcp.o: $(OBJ)
	$(LD) -r $(OBJ) -o $@
	$(OBJCOPY) $(OCPFLAGS) $@

src/les2htcp.d src/les2htcp.o: config.mk

src/les2htcp.c: src/les2htcp.h
src/les2htcp.h: src/les2htcp.h.in
	sed -e 's#@VERSION_MAJOR@#$(VERSION_MAJOR)#g' \
	    -e 's#@VERSION_MINOR@#$(VERSION_MINOR)#g' \
	    -e 's#@VERSION_PATCH@#$(VERSION_PATCH)#g' \
	    src/les2htcp.h.in > $@

les2htcp: src/les2htcp.o
	$(CC) $(CFLAGS_EXE) $(RSYS_CFLAGS) $(NETCDF_CFLAGS) \
	-o $@ src/les2htcp.o $(LDFLAGS_EXE) $(RSYS_LIBS) $(NETCDF_LIBS) -lm

.config: config.mk
	@if ! $(PKG_CONFIG) --atleast-version $(RSYS_VERSION) rsys; then \
	  echo "rsys $(RSYS_VERSION) not found" >&2; exit 1; fi
	@if ! $(PKG_CONFIG) --atleast-version $(NETCDF_VERSION) netcdf; then \
	  echo "netcdf $(NETCDF_VERSION) not found" >&2; exit 1; fi
	@echo "config done" > $@

.SUFFIXES: .c .d .o
.c.d:
	@$(CC) $(CFLAGS_SO) $(RSYS_CFLAGS) -MM -MT "$(@:.d=.o) $@" $< -MF $@

.c.o:
	$(CC) $(CFLAGS_SO) $(RSYS_CFLAGS) -DHTCP_SHARED_BUILD -c $< -o $@

src/les2htcp.d: src/les2htcp.c
	@$(CC) $(CFLAGS_EXE) $(RSYS_CFLAGS) $(NETCDF_CFLAGS) -MM -MT "$(@:.d=.o) $@" \
	src/les2htcp.c -MF $@

src/les2htcp.o: src/les2htcp.c
	$(CC) $(CFLAGS_EXE) $(RSYS_CFLAGS) $(NETCDF_CFLAGS) -c src/les2htcp.c -o $@

################################################################################
# Installation
################################################################################
pkg:
	sed -e 's#@PREFIX@#$(PREFIX)#g'\
	    -e 's#@VERSION@#$(VERSION)#g'\
	    -e 's#@RSYS_VERSION@#$(RSYS_VERSION)#g'\
	    htcp.pc.in > htcp.pc

htcp-local.pc: htcp.pc.in
	sed -e '1d'\
	    -e 's#^includedir=.*#includedir=./src/#'\
	    -e 's#^libdir=.*#libdir=./#'\
	    -e 's#@VERSION@#$(VERSION)#g'\
	    -e 's#@RSYS_VERSION@#$(RSYS_VERSION)#g'\
	    htcp.pc.in > $@

install: build_library build_program pkg
	@$(SHELL) make.sh install "$(DESTDIR)$(PREFIX)/bin" les2htcp
	@$(SHELL) make.sh install "$(DESTDIR)$(PREFIX)/lib" $(LIBNAME)
	@$(SHELL) make.sh install "$(DESTDIR)$(PREFIX)/lib/pkgconfig" htcp.pc
	@$(SHELL) make.sh install "$(DESTDIR)$(PREFIX)/include/high_tune" src/htcp.h
	@$(SHELL) make.sh install "$(DESTDIR)$(PREFIX)/share/doc/htcp" COPYING README.md
	@$(SHELL) make.sh install "$(DESTDIR)$(PREFIX)/share/man/man1" les2htcp.1
	@$(SHELL) make.sh install "$(DESTDIR)$(PREFIX)/share/man/man5" htcp.5

uninstall:
	rm -f "$(DESTDIR)$(PREFIX)/bin/les2htcp"
	rm -f "$(DESTDIR)$(PREFIX)/lib/$(LIBNAME)"
	rm -f "$(DESTDIR)$(PREFIX)/lib/pkgconfig/htcp.pc"
	rm -f "$(DESTDIR)$(PREFIX)/share/doc/htcp/COPYING"
	rm -f "$(DESTDIR)$(PREFIX)/share/doc/htcp/README.md"
	rm -f "$(DESTDIR)$(PREFIX)/include/high_tune/htcp.h"
	rm -f "$(DESTDIR)$(PREFIX)/share/man/man1/les2htcp.1"
	rm -f "$(DESTDIR)$(PREFIX)/share/man/man5/htcp.5"

################################################################################
# Miscellaneous targets
################################################################################
all: build_library build_program build_tests

clean: clean_test
	rm -f $(OBJ) src/les2htcp.o $(TEST_OBJ) $(LIBNAME) les2htcp
	rm -f .config .test libhtcp.o htcp.pc htcp-local.pc

distclean: clean
	rm -f $(DEP) src/les2htcp.d src/les2htcp.h $(TEST_DEP)

lint:
	shellcheck -o all make.sh
	shellcheck -o all src/dump_netcdf_data.sh
	shellcheck -o all src/dump_netcdf_desc.sh
	shellcheck -o all src/test_htcp_load_from_file.sh
	mandoc -Tlint -Wall les2htcp.1 || [ $$? -le 1 ]
	mandoc -Tlint -Wall htcp.5 || [ $$? -le 1 ]

################################################################################
# Tests
################################################################################
TEST_SRC =\
 src/test_htcp.c\
 src/test_htcp_load.c\
 src/test_htcp_load_from_file.c
TEST_OBJ = $(TEST_SRC:.c=.o)
TEST_DEP = $(TEST_SRC:.c=.d)

PKG_CONFIG_LOCAL = PKG_CONFIG_PATH="./:$${PKG_CONFIG_PATH}" $(PKG_CONFIG)
HTCP_CFLAGS = $$($(PKG_CONFIG_LOCAL) $(PCFLAGS) --cflags htcp-local.pc)
HTCP_LIBS = $$($(PKG_CONFIG_LOCAL) $(PCFLAGS) --libs htcp-local.pc)

test: build_tests build_program
	@$(SHELL) make.sh run_test src/test_htcp.c src/test_htcp_load.c

build_tests: build_library $(TEST_DEP) .test
	@$(MAKE) -fMakefile -f.test $$(for i in $(TEST_DEP); do echo -f"$${i}"; done) test_bin

.test: Makefile make.sh
	@$(SHELL) make.sh config_test $(TEST_SRC) > $@

clean_test:
	$(SHELL) make.sh clean_test $(TEST_SRC)

$(TEST_DEP): config.mk htcp-local.pc
	@$(CC) $(CFLAGS_EXE) $(HTCP_CFLAGS) $(RSYS_CFLAGS) \
	-MM -MT "$(@:.d=.o) $@" $(@:.d=.c) -MF $@

$(TEST_OBJ): config.mk htcp-local.pc
	$(CC) $(CFLAGS_EXE) $(HTCP_CFLAGS) $(RSYS_CFLAGS) -c $(@:.o=.c) -o $@

test_htcp \
test_htcp_load \
test_htcp_load_from_file \
: config.mk htcp-local.pc $(LIBNAME)
	$(CC) $(CFLAGS_EXE) -o $@ src/$@.o $(LDFLAGS_EXE) $(HTCP_LIBS) $(RSYS_LIBS) -lm
