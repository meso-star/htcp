#!/bin/sh -e

# Copyright (C) 2018, 2020-2023 |Méso|Star> (contact@meso-star.com)
# Copyright (C) 2018 Centre National de la Recherche Scientifique
# Copyright (C) 2018 Université Paul Sabatier
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>. */
set -e

if [ $# -lt 1 ]; then
  printf "usage: %s variable netcdf\n" "${0##*/}" >&2
  exit 1
fi


b="[[:blank:]]\{0,\}" # Blanks
c=".\{0,\}" # Any chars

dimensions=$(ncdump -h "$1" | sed "/^${b}variables/,\$d" | sed '1,2d')
nx=$(echo "${dimensions}" | \
  sed -n "s/^${c}W_E_direction${b}=${b}\([0-9]\{1,\}\)${b};${c}$/\1/p")
ny=$(echo "${dimensions}" | \
  sed -n "s/^${c}S_N_direction${b}=${b}\([0-9]\{1,\}\)${b};${c}$/\1/p")
nz=$(echo "${dimensions}" | \
  sed -n "s/^${c}vertical_levels${b}=${b}\([0-9]\{1,\}\)${b};${c}$/\1/p")
ntimes=$(echo "${dimensions}" | \
  sed -n "s/^${c}time${b}=${b}\([0-9]\{1,\}\)${b};${c}$/\1/p")

if [ -z "${ntimes}" ]; then
  ntimes=$(echo "${dimensions}" | \
  sed -n "s/^${c}time${b}=${c}\/\/${b}(\([0-9]\{1,\}\) currently)${c}$/\1/p")
fi

if [ -z "${nx}" ] \
|| [ -z "${ny}" ] \
|| [ -z "${nz}" ] \
|| [ -z "${ntimes}" ]
then
  >&2 printf "%s: error retrieving the dimensions of \"%s\"\n" "$0" "$1"
  exit 1
fi

name=$(basename "$1")
name=${name%.*}
{
  echo "${nx}"
  echo "${ny}"
  echo "${nz}"
  echo "${ntimes}"
} > "${name}_desc"
