#!/bin/sh -e

# Copyright (C) 2018, 2020-2023 |Méso|Star> (contact@meso-star.com)
# Copyright (C) 2018 Centre National de la Recherche Scientifique
# Copyright (C) 2018 Université Paul Sabatier
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>. */

dump_data() {
  variable="$1"
  netcdf="$2"
  printf "Dump the variable \"%s\" from \"%s\"\n" "${variable}" "${netcdf}"
  dump_netcdf_data.sh "${variable}" "${netcdf}"
}

if [ $# -ne 1 ]; then
  printf "usage: %s netcdf\n" "${0##*/}" >&2
  exit 1
fi

nc="$1"

if [ ! -f "${nc}" ]; then
  printf "Invalid netcdf: %s\n" "${nc}" >&2
  exit 1
fi

prefix=$(basename "${nc}")
prefix=${prefix%.*}
htcp="${prefix}.htcp"
path=$(pwd)

printf "Convert \"%s\" to \"%s\"\n" "${nc}" "${htcp}"
les2htcp -m 1000 -i "${nc}" -fo "${htcp}"

printf "Dump the descriptor of \"%s\"\n" "${nc}"
dump_netcdf_desc.sh "${nc}"

dump_data "PABST" "${nc}"
dump_data "RCT" "${nc}"
dump_data "RVT" "${nc}"
dump_data "THT" "${nc}"

test_htcp_load_from_file "${prefix}.htcp" "${path}"
