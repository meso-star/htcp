#!/bin/sh -e

# Copyright (C) 2018, 2020-2023 |Méso|Star> (contact@meso-star.com)
# Copyright (C) 2018 Centre National de la Recherche Scientifique
# Copyright (C) 2018 Université Paul Sabatier
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>. */

set -e

if [ $# -lt 2 ]; then
  printf "usage: %s variable netcdf\n" "${0##*/}" >&2
  exit 1
fi

if [ ! -f "$2" ]; then
  printf "\"%s\" is not a valid file\n" "$2" >&2
  exit 1
fi

name=$(basename "$2")
name=${name%.*}

blanks="[[:blank:]]\{0,\}"
ncdump -v "$1" "$2" \
  | sed -n "/^${blanks}$1${blanks}=/,\$p" \
  | sed "s/^${blanks}$1${blanks}=${blanks}//g" \
  | sed "s/[;} ]//g" \
  | sed "s/,/\n/g" \
  | sed "/^${blanks}$/d" > "${name}_${1}"
